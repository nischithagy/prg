/**
     *@NApiVersion 2.x
    *@NScriptType ClientScript
    */
/***************************************************************************************
 ** Copyright (c) 1998-2021 Softype, Inc.
** Ventus Infotech Private Limited, 
** All Rights Reserved.
**
** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
** the license agreement you entered into with Softype.
**
** @Author      :  Nischitha G Y
** @Dated       :  29 March 2022
** @Version     :  2.x
**@NModuleScope :public
** @Description :  Pending Liqudation for employee
***************************************************************************************/
define(['N/error', 'N/search', 'N/log', 'N/record', 'N/url'],
    function (error, search, log, record, url) {
        function fieldChanged(context) {
            try {
                var objRecord = context.currentRecord;
                var type = context.type;
                var id = context.id;
                if (context.fieldId == "custrecord_travel_employee") {

                    var employee_id = objRecord.getValue({ fieldId: 'custrecord_travel_employee' }) || '';
                    log.debug("employee_id", employee_id);
                    if (employee_id == '') {

                        window.onbeforeunload = null;

                        window.location.replace('https://4872008-sb2.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=103');


                    }


                }

            } catch (e) {

                alert(e)
            }

        }
        function validField(context) {

            var objRecord = context.currentRecord;
            var type = context.type;
            var id = context.id;
            if (context.fieldId == "custrecord_travel_employee") {

                var employee_id = objRecord.getValue({ fieldId: 'custrecord_travel_employee' });
                log.debug("employee_id", employee_id);
                if (employee_id) {
                    var employeeName = search.lookupFields({
                        type: "employee",
                        id: employee_id,
                        columns: ['custentity_employeename']

                    });
                    log.debug("lookup", employeeName.custentity_employeename);

                    if (employeeName.custentity_employeename) {

                        const customrecord_cashadvancereqSearchColEmployee = search.createColumn({ name: 'custrecord_travel_employee', sort: search.Sort.ASC });
                        const customrecord_cashadvancereqSearchColEmployeeName = search.createColumn({ name: 'custrecord_travel_employeename' });
                        const customrecord_cashadvancereqSearch = search.create({
                            type: 'customrecord_cashadvancereq',
                            filters: [
                                ['custrecord_travel_status', 'anyof', '2'],
                                'AND',
                                ['custrecordcustrecord_travel_empexp', 'anyof', '2'],
                                'AND',
                                ['custrecord_travel_voided', 'is', 'F'],
                                'AND',
                                ["custrecord14", "onorbefore", "today"],
                                'AND',
                                ['custrecord_travel_employeename', 'is', employeeName.custentity_employeename],
                            ],
                            columns: [
                                customrecord_cashadvancereqSearchColEmployee,
                                customrecord_cashadvancereqSearchColEmployeeName,
                            ],
                        });

                        var objSearch_run = customrecord_cashadvancereqSearch.run().getRange({
                            start: 0,
                            end: 100
                        });
                        log.debug("objSearch_run.length", objSearch_run.length)

                        if (objSearch_run.length == 0) {
                            return true;
                        }
                        else if (objSearch_run.length < 2 && objSearch_run.length > 0) {
                            alert("With due for liquidation, Would you like to proceed?");
                            return true;

                        } else if (objSearch_run.length >= 2) {
                            alert("With due for liquidation 2 or more records");
                            return false;
                        }


                    }
                }


            }
            return true;

        }

        function writecheck(recordId) {

            log.debug("recordId", recordId)

            var recordObj = record.load({
                type: 'customrecord_cashadvancereq',
                id: recordId

            })
            log.debug("recordObj", recordObj)

            var status = recordObj.getValue({ fieldId: 'custrecord_travel_status' });
            var checkLink = recordObj.getValue({ fieldId: 'custrecord_prg_check_link' }) || '';
            log.debug("checkLink", checkLink)
            var checkSearchObj = search.create({
                type: "check",
                filters:
                    [
                        ["type", "anyof", "Check"],
                        "AND",
                        ["mainline", "is", "T"],
                        "AND",
                        ["custbody_prg_check_cash_adv", "anyof", recordId]
                    ],
                columns:
                    [
                        search.createColumn({ name: "custbody_prg_check_cash_adv", label: "Cash Advance Request" })
                    ]
            });
            var searchResultCount = checkSearchObj.runPaged().count;
            log.debug("checkSearchObj result count", searchResultCount);
            var cashid = '';
            checkSearchObj.run().each(function (result) {
                cashid = result.getValue('custbody_prg_check_cash_adv');
                return true;
            });
            log.debug("cashid ", cashid);

            if (status == 2 && checkLink == '') {

                // if (!checkLink) {

                var employee_id = recordObj.getValue({ fieldId: 'custrecord_travel_employee' });

                var employeeName = search.lookupFields({
                    type: "employee",
                    id: employee_id,
                    columns: ['custentity_employeename']

                });

                if (employeeName.custentity_employeename) {

                    const customrecord_cashadvancereqSearchColEmployee = search.createColumn({ name: 'custrecord_travel_employee', sort: search.Sort.ASC });
                    const customrecord_cashadvancereqSearchColEmployeeName = search.createColumn({ name: 'custrecord_travel_employeename' });
                    const customrecord_cashadvancereqSearch = search.create({
                        type: 'customrecord_cashadvancereq',
                        filters: [
                            ['custrecord_travel_status', 'anyof', '2'],
                            'AND',
                            ['custrecordcustrecord_travel_empexp', 'anyof', '2'],
                            'AND',
                            ['custrecord_travel_voided', 'is', 'F'],
                            'AND',
                            ["custrecord14", "onorbefore", "today"],
                            'AND',
                            ['custrecord_travel_employeename', 'is', employeeName.custentity_employeename],
                        ],
                        columns: [
                            customrecord_cashadvancereqSearchColEmployee,
                            customrecord_cashadvancereqSearchColEmployeeName,
                        ],
                    });

                    var objSearch_run = customrecord_cashadvancereqSearch.run().getRange({
                        start: 0,
                        end: 100
                    });
                    // alert(objSearch_run.length)

                    if (objSearch_run.length > 1) {
                        alert("With due for liquidation");

                    }
                }

                //}
                var subsidiary = recordObj.getValue("custrecord_travel_subsidiary");
                log.debug('subsidiary', subsidiary);
                var entity = recordObj.getValue("custrecord_travel_employee");
                log.debug('entity', entity);
                var location1 = recordObj.getValue("custrecord_travel_location");
                log.debug('location1', location1);
                var amount = recordObj.getValue("custrecord_travel_budget");
                log.debug('amount', amount);
                var printOnCheck = recordObj.getValue("custrecord_travel_employeename");
                log.debug('printOnCheck', printOnCheck);

                var fieldLookUpSub = search.lookupFields({
                    type: "subsidiary",
                    id: subsidiary,
                    columns: ['custrecord_defaultbankacc', 'custrecord_expensecatergory']
                });
                log.debug('fieldLookUpSub', fieldLookUpSub);

                var account = fieldLookUpSub.custrecord_defaultbankacc[0].value;;
                log.debug('account', account);

                var category = fieldLookUpSub.custrecord_expensecatergory[0].value;;
                log.debug('category', category);

                try {
                    if (cashid == '') {
                        var checkObj = record.create({
                            type: "check",
                            isDynamic: true,
                            defaultValues: {
                                'entity': entity
                            }
                        });

                        checkObj.setValue({
                            fieldId: 'customform',
                            value: 103
                        });

                        var fieldLookUpExpense = search.lookupFields({
                            type: "expensecategory",
                            id: category,
                            columns: ['name', 'account']
                        });

                        log.debug('fieldLookUpExpense', fieldLookUpExpense);
                        var name = fieldLookUpExpense.name;
                        log.debug('name', name);

                        var expenseacc = fieldLookUpExpense.account[0].value;
                        log.debug('expenseacc', expenseacc);


                        checkObj.setValue({
                            fieldId: 'custbody_printcheck',
                            value: printOnCheck
                        });

                        checkObj.setValue({
                            fieldId: 'location',
                            value: location1
                        });

                        checkObj.selectLine({
                            sublistId: 'expense',
                            line: 0

                        });

                        checkObj.setCurrentSublistValue({
                            sublistId: 'expense',
                            fieldId: 'account',
                            value: expenseacc
                        });

                        checkObj.setCurrentSublistValue({
                            sublistId: 'expense',
                            fieldId: 'amount',
                            value: amount
                        });
                        checkObj.setCurrentSublistValue({
                            sublistId: 'expense',
                            fieldId: 'taxcode',
                            value: 5
                        });

                        checkObj.setCurrentSublistValue({
                            sublistId: 'expense',
                            fieldId: 'location',
                            value: location1
                        });
                        checkObj.commitLine({
                            sublistId: 'expense'
                        });

                        checkObj.setValue({
                            fieldId: 'custbody_prg_check_cash_adv',
                            value: recordId
                        });
                        var Id = checkObj.save() || '';
                        if (Id != '') {

                            recordObj.setValue('custrecord_prg_check_link', Id)
                            var Objid = recordObj.save()

                        }
                    } else {
                        recordObj.setValue('custrecord_prg_check_link', cashid);
                        var Objid = recordObj.save()
                    }

                    log.debug({
                        title: "id",
                        details: Id
                    })

                    window.location.href = "https://4872008-sb2.app.netsuite.com/app/common/custom/custrecordentry.nl?rectype=103&id=" + Objid


                }
                catch (e) {
                    log.error('e', e)

                }
            }

        }

        return {

            validateField: validField,//validateField
            writecheck: writecheck,
            fieldChanged: fieldChanged
            
        };
    });