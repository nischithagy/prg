/**
 *@NApiVersion 2.x
*@NScriptType ScheduledScript
*/
/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
**You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
**                      
**@Author      :  Nischitha G Y
**@Dated       :  17 MAY 2022
**@Version     :  2.x
**@Description :  Scheduled Script to send email to employee whose due date is near to cash advance request
***************************************************************************************/
define(['N/record', 'N/task', 'N/file', 'N/runtime', 'N/search', 'N/format', 'N/render', 'N/email'],
    function (record, task, file, runtime, search, format, render, email) {
        function execute(context) {

            try {
                var results = search.load({
                    id: 'customsearch7623'
                });

                var resultSet = results.run().getRange(0, 1000);

                log.debug('resultssearch', resultSet.length);
                for (var i = 0; i < resultSet.length; i++) {

                    var internalId = resultSet[i].id;
                    log.debug('Internal id is', internalId);

                    var fieldLookUp = search.lookupFields({
                        type: "customrecord_cashadvancereq",
                        id: internalId,
                        columns: ['custrecord15', 'custrecord_travel_employee']
                    });
                    log.debug('fieldLookUp is', fieldLookUp);

                    var emailId = fieldLookUp.custrecord15;

                    var employeeId = fieldLookUp.custrecord_travel_employee[0].value;
                    log.debug('employeeId is', employeeId);

                    var mergeResult = render.mergeEmail({
                        templateId: 213,
                        entity: null,
                        recipient: {
                            type: 'employee',
                            id: parseInt(employeeId)
                        },
                        supportCaseId: null,
                        transactionId: null,
                        customRecord: null
                    });

                    var emailSent = email.send({
                        author: 83100,
                        recipients: emailId,
                        body: mergeResult.body,
                        subject: mergeResult.subject
                    });
                    log.debug('emailSent', emailSent);

                }

            } catch (e) {

                log.error(e)

            }
        }


        return {
            execute: execute
        }

    });