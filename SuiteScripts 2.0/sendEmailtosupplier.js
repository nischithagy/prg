/**
 *@NApiVersion 2.0
 *@NScriptType workflowactionscript
 */
/***************************************************************************************
 ** Copyright (c) 1998-2020 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 **
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
 ** the license agreement you entered into with Softype.
 **
 **@Author  : Nischitha
 **@Dated   : 08/03/2022
 **@Version :   2.0
 **@Description :  Workflow Action script On PR Record For Approval Matrix 
 ***************************************************************************************/
define(['N/record', 'N/search', "N/log", "N/url", "N/runtime", 'N/render', 'N/email'],
    function (record, search, log, url, runtime, render, email) {

        function setPostStatus(scriptContext) {
            try {
                var newRecord = scriptContext.newRecord;
                var recType = newRecord.type;
                var recid = newRecord.id;
                // var userObj = runtime.getCurrentUser().id;
                // log.debug('userObj',userObj);

                if (recType == 'purchaseorder' || recType == 'PURCHASEORDER') {

                    var custaprovalStatus = newRecord.getValue({
                        fieldId: 'custbody_transaction_app_status'
                    });
                    log.debug('custaprovalStatus', custaprovalStatus);

                    // var aprovalStatus= newRecord.getValue({
                    //     fieldId: 'approvalstatus'
                    // });
                    // log.debug('aprovalStatus',aprovalStatus);

                    var entity = newRecord.getValue({
                        fieldId: 'entity'
                    });
                    log.debug('entity', entity);

                    if (custaprovalStatus == "2") {

                        var fieldLookUp = search.lookupFields({
                            type: "vendor",
                            id: entity,
                            columns: ['email']
                        });

                        var emailId = fieldLookUp.email;
                        log.debug('emailId', emailId);
                        var mergeResult = render.mergeEmail({
                            templateId: 207,
                            entity: {
                                type: 'employee',
                                id: parseInt(recid)
                            },
                            recipient: {
                                type: 'vendor',
                                id: parseInt(recid)
                            },
                            supportCaseId: null,
                            transactionId: recid,
                            customRecord: null
                        });
                        var recPDF = render.transaction({
                            entityId: recid,
                            printMode: render.PrintMode.PDF,

                        });
                        var senderId = 98137;//83301 in sandbox

                        var emailSent = email.send({
                            author: senderId,
                            recipients: emailId,
                            body: mergeResult.body,
                            attachments: [recPDF],
                            subject: mergeResult.subject

                        });

                        log.debug('emailSent', emailSent);



                    }
                }

            } catch (e) {
                log.error('error', e);

            }



        }

        return {
            onAction: setPostStatus
        };
    });