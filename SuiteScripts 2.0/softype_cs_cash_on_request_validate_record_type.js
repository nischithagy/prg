/**
     *@NApiVersion 2.x
    *@NScriptType ClientScript
    */
/***************************************************************************************
 ** Copyright (c) 1998-2021 Softype, Inc.
** Ventus Infotech Private Limited, 
** All Rights Reserved.
**
** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
** You shall not disclose such Confidential Information and shall use it only in accordance with the terms of
** the license agreement you entered into with Softype.
**
** @Author      :  Nischitha G Y
** @Dated       :  29 March 2022
** @Version     :  2.x
** @Description :  Pending Liqudation for employee
***************************************************************************************/
define(['N/error', 'N/search'],
    function (error, search) {

        function validField(context) {

            var objRecord = context.currentRecord;
            var type = context.type;
            var id = context.id;
            if (context.fieldId == "custrecord_travel_employee") {

                var employee_id = objRecord.getValue({ fieldId: 'custrecord_travel_employee' });
                log.debug("employee_id", employee_id);
                var employeeName = search.lookupFields({
                    type: "employee",
                    id: employee_id,
                    columns: ['custentity_employeename', 'custentity5']

                });
                log.debug("lookup", employeeName.custentity_employeename);
                log.debug("lookupRecord", employeeName.custentity5);
                var record_type=employeeName.custentity5[0].value;
                log.debug("record_type", record_type);
                if (record_type== "2") {

                    if (employeeName.custentity_employeename) {

                        const customrecord_cashadvancereqSearchColEmployee = search.createColumn({ name: 'custrecord_travel_employee', sort: search.Sort.ASC });
                        const customrecord_cashadvancereqSearchColEmployeeName = search.createColumn({ name: 'custrecord_travel_employeename' });
                        const customrecord_cashadvancereqSearch = search.create({
                            type: 'customrecord_cashadvancereq',
                            filters: [
                                ['custrecord_travel_status', 'anyof', '2'],
                                'AND',
                                ['custrecordcustrecord_travel_empexp', 'anyof', '2'],
                                'AND',
                                ['custrecord_travel_voided', 'is', 'F'],
                                'AND',
                                ["custrecord14", "onorbefore", "today"],
                                'AND',
                                ['custrecord_travel_employeename', 'is', employeeName.custentity_employeename],
                            ],
                            columns: [
                                customrecord_cashadvancereqSearchColEmployee,
                                customrecord_cashadvancereqSearchColEmployeeName,
                            ],
                        });

                        var objSearch_run = customrecord_cashadvancereqSearch.run().getRange({
                            start: 0,
                            end: 1000
                        });
                        log.debug("objSearch_run.length", objSearch_run.length)

                        if (objSearch_run.length == 0) {
                            return true;
                        }
                        else if (objSearch_run.length < 2 && objSearch_run.length > 0) {
                            alert("With due for liquidation, Would you like to proceed?");
                            return true;

                        } else if (objSearch_run.length >= 2) {
                            alert("With due for liquidation 2 or more records");
                            return false;
                        }


                    }
                }
                else {
                    alert("Invalid employee record type use for cash advance");
                    return false;
                }


            }
            return true;


        }
        return {

            validateField: validField
        };
    });