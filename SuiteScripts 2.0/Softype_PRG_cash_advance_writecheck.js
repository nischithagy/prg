/**
 *@NApiVersion 2.x
*@NScriptType UserEventScript
*@NModuleScope public
*/
/***************************************************************************************  
** Copyright (c) 1998-2018 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
**You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
**                      
**@Author      :  Nischitha G Y
**@Dated       :  25 april 2022
**@Version     :  2.x
**@Description :  create a button on cash advance request if status is approved. 
**@Updates     :  create a button on cash advance request if status is approved. 
***************************************************************************************/
define(['N/error', 'N/record'],
    function (err, record) {
        function beforeLoad(context) {
            try {
                var form = context.form;
                var currentRecordObj = context.newRecord;
                var type = context.newRecord.type;
                var recordId = context.newRecord.id;

                var status = currentRecordObj.getValue({ fieldId: 'custrecord_travel_status' });

                var checkLink = currentRecordObj.getValue({ fieldId: 'custrecord_prg_check_link' }) || '';
                // if(checkLink){
                //     buttonObj.isHidden=true;

                // }
                log.debug("context.UserEventType", context.type)
                if (status == 2 && context.type == context.UserEventType.VIEW && checkLink == '') {


                    // context.form.clientScriptModulePath = 'SuiteScripts/softype_cs_cash_on_request_validate.js';
                    form.clientScriptFileId = 4364882;
                    var buttonObj = form.addButton({
                        id: 'custpage_appove',
                        label: 'Write Check',
                        functionName: 'writecheck(' + recordId + ')'
                    })


                }

            }
            catch (e) {
                log.error('error in beforeLoad function', e);
            }
        }

        return {
            beforeLoad: beforeLoad

        };
    });