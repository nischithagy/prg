/**
*@NApiVersion 2.x
*@NScriptType ScheduledScript
*/
/***************************************************************************************  
** Copyright (c) 1998-2018 Softype, Inc.
** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
** All Rights Reserved.
** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
**You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
**                      
**@Author      :  Jaydas Sakhare
**@Dated       :  05 June 2019	
**@Version     :  2.x
**@Description :  JDA NS scheduled script to create sales invoice from CSV file. 
**@Updates     :  Completely modified by Nischitha on 12th May 2022.
***************************************************************************************/
define(['N/record', 'N/search', 'N/runtime', 'N/log', 'N/file', 'N/transaction', 'N/format', 'N/task'], function (record, search, runtime, log, file, transaction, format, task) {

    function execute(scriptContext) {

        var scriptObj = runtime.getCurrentScript();
        var lastDocProcess = scriptObj.getParameter({ name: 'custscriptsoftype_hyper_mart_jda_ns_rdjv' });

        var credit_memo_unprocessed_folder = 25801;
        var credit_memo_processed_folder = 25800;

        var fileName = '';
        var currentDocNo = '';

        if (runtime.envType === 'PRODUCTION') {
            var credit_memo_unprocessed_folder = 25801;
            var credit_memo_processed_folder = 25800;

        }

        // Fetch all the files available in sales -> unprocessed folder
        var unprocessedFileSearchObj = search.create({
            type: "file",
            filters: [
                ["folder", "anyof", credit_memo_unprocessed_folder]
            ],
            columns: [
                search.createColumn({
                    name: "internalid",
                    sort: search.Sort.ASC,
                    label: "Internal Id"
                }),
                search.createColumn({ name: "name", label: "Name" }),
                search.createColumn({ name: "filetype", label: "Type" })
            ]
        });

        // Loop through all the files found
        unprocessedFileSearchObj.run().each(function (fileRow) {
            // .run().each has a limit of 4,000 results
            /* Columns Order of CSV file:- 0 Store, 1 Date, 2 Customer Number , 3 Customer Name, //4 Payment Code, 4 Payment Desc,// 6 Netsuite Code, 5 Payment Amount, 6 Document Number, 7 Subsidiary */

            var fileId = fileRow.id;
            fileName = fileRow.getValue('name');
            //  log.debug('fileName', fileName);
            var salesDataFileObj = file.load({
                id: fileId
            });

            // Iterate through lines of CSV file and store data in object of arrays with Document number as its group key.
            var SalesData = {};
            salesDataFileObj.lines.iterator().each(function (line) {
                var data = line.value.split(",");
                if (isNaN(data[0])) {
                    return true;
                }

                if (data[6]) {
                    var tranId = data[6].trim();
                    // log.debug("tranId", tranId)

                    if (SalesData[tranId]) {
                        SalesData[tranId].push(data);
                    } else {
                        SalesData[tranId] = [];
                        SalesData[tranId].push(data);
                    }
                }

                return true;
            });

            // Iterate through sales data and remove the document numbers until the Last Document Process in order to continue from Last Document processed in case of script rescheduled.
            for (var tranId in SalesData) {
                if (lastDocProcess) {
                    if (lastDocProcess == tranId) {
                        break;
                    }
                } else {
                    break;
                }

                delete SalesData[tranId];
            }

            // Iterate through sales data and create JDA NS credit memo redeem custom record
            for (var key in SalesData) {
                if (key == '') {
                    continue;
                }

                var recordData = SalesData[key];

                try {
                    if (recordData.length > 0 && recordData[0]) {

                        var bodyLevelData = recordData[0];
                        if (bodyLevelData[6]) {
                            currentDocNo = bodyLevelData[6].trim();
                            //log.debug("currentDocNo", currentDocNo)
                        }


                        if (runtime.getCurrentScript().getRemainingUsage() > 100) {

                            // var customrecord_jda_ns_cm_redeemSearchObj = search.create({
                            //     type: "customrecord_jda_ns_cm_redeem",
                            //     filters:
                            //         [
                            //             ["custrecord_cmr_document_no", "is", bodyLevelData[6]]
                            //         ],
                            //     columns:
                            //         [
                            //             search.createColumn({ name: "custrecord_cmr_document_no", label: "Document No." })
                            //         ]
                            // });
                            // var searchResultCount = customrecord_jda_ns_cm_redeemSearchObj.runPaged().count;
                            // //log.debug("customrecord_jda_ns_cm_redeemSearchObj result count", searchResultCount);

                            // if (searchResultCount == 0) {

                                // Set Body Level fields in the JDA NS credit memo redeem
                                var creditmemoObj = record.create({
                                    type: "customrecord_jda_ns_cm_redeem",
                                    isDynamic: true
                                });


                                if (bodyLevelData[6]) {
                                    currentDocNo = bodyLevelData[6].trim();
                                    creditmemoObj.setValue({
                                        fieldId: 'custrecord_cmr_document_no',
                                        value: bodyLevelData[6].trim()
                                    });
                                }

                                if (bodyLevelData[0]) {
                                    creditmemoObj.setText({
                                        fieldId: 'custrecord_cmr_location',
                                        text: bodyLevelData[0].trim()
                                    });
                                }

                                if (bodyLevelData[1]) {
                                    var trandate = format.parse({
                                        value: bodyLevelData[1],
                                        type: format.Type.DATE
                                    });


                                    if (trandate) {
                                        creditmemoObj.setValue({
                                            fieldId: 'custrecord_cmr_date',
                                            value: trandate
                                        });
                                    }
                                }

                                // Customer Number
                                if (bodyLevelData[2]) {
                                    creditmemoObj.setText({
                                        fieldId: 'custrecord_cmr_customerno',
                                        text: bodyLevelData[2].trim()
                                    });
                                }

                                //
                                if (bodyLevelData[3]) {
                                    creditmemoObj.setText({
                                        fieldId: 'custrecord_cmr_customer_name',
                                        text: bodyLevelData[3].trim()
                                    });
                                }

                                if (bodyLevelData[4]) {
                                    creditmemoObj.setText({
                                        fieldId: 'custrecord_cmr_payment_descr',
                                        text: bodyLevelData[4].trim()
                                    });
                                }

                                if (bodyLevelData[5]) {
                                    creditmemoObj.setText({
                                        fieldId: 'custrecord_cmr_payment_amount',
                                        text: bodyLevelData[5].trim()
                                    });
                                }

                                if (bodyLevelData[7]) {
                                    creditmemoObj.setValue({
                                        fieldId: 'custrecord_cmr_subsidiary',
                                        value: bodyLevelData[7].trim()
                                    });
                                }


                                // Save the invoice
                                var creditId = creditmemoObj.save();
                                log.debug('New creditId', creditId);

                            // } else {

                            //     var errorMessage = "duplicate document number";
                            //     // Create a new error catcher record and save the error details.
                            //     var erroCatcherObj = record.create({
                            //         type: 'customrecord_jda_ns_error_catcher'
                            //     });
                            //     erroCatcherObj.setValue({
                            //         fieldId: 'custrecord_jda_ns_ec_name',
                            //         value: 'Sales CSV upload error'
                            //     });
                            //     erroCatcherObj.setValue({
                            //         fieldId: 'custrecord_jda_ns_ec_error',
                            //         value: errorMessage
                            //     });
                            //     erroCatcherObj.setValue({
                            //         fieldId: 'custrecord_jda_ns_ec_file_name',
                            //         value: fileName
                            //     });
                            //     erroCatcherObj.setValue({
                            //         fieldId: 'custrecord_jda_ns_ec_doc_no',
                            //         value: currentDocNo
                            //     });
                            //     var errorid = erroCatcherObj.save();
                            //     log.debug('errorid -->', errorid);


                            // }
                        }
                        else {
                            rescheduleCurrentScript(currentDocNo);
                            log.debug('Remaining Usage', runtime.getCurrentScript().getRemainingUsage());
                            return false;
                        }

                    }
                } catch (e) {

                    var errorMessage = e.message;
                    // Create a new error catcher record and save the error details.
                    var erroCatcherObj = record.create({
                        type: 'customrecord_jda_ns_error_catcher'
                    });
                    erroCatcherObj.setValue({
                        fieldId: 'custrecord_jda_ns_ec_name',
                        value: 'Sales CSV upload error'
                    });
                    erroCatcherObj.setValue({
                        fieldId: 'custrecord_jda_ns_ec_error',
                        value: errorMessage
                    });
                    erroCatcherObj.setValue({
                        fieldId: 'custrecord_jda_ns_ec_file_name',
                        value: fileName
                    });
                    erroCatcherObj.setValue({
                        fieldId: 'custrecord_jda_ns_ec_doc_no',
                        value: currentDocNo
                    });
                    var errorid = erroCatcherObj.save();
                    log.debug('errorid -->', errorid);

                }
            }

            if (salesDataFileObj) {
                // Create a copy of the current csv file in processed folder and delete the original file from unprocessed folder.
                var processedSalesDataFileObj = file.create({
                    name: salesDataFileObj.name,
                    fileType: file.Type.CSV,
                    contents: salesDataFileObj.getContents(),
                    folder: credit_memo_processed_folder
                });
                var newFileId = processedSalesDataFileObj.save();
                if (newFileId && fileId) {
                    file.delete({
                        id: fileId
                    });
                }
            }

            return true;
        });

    }

    function rescheduleCurrentScript(lastDocProcess) {
        log.debug('lastDocProcess', lastDocProcess);
        var ssTask = task.create({
            taskType: task.TaskType.SCHEDULED_SCRIPT,
            scriptId: 'customscript_ss_create_returns',
            deploymentId: 'customdeploy_ss_create_returns',
            params: {
                custscriptsoftype_hyper_mart_jda_ns_rdjv: lastDocProcess,
            }
        });

        return ssTask.submit();
    }

    return {
        execute: execute
    }

});