/**
 *@NApiVersion 2.x
 *@NScriptType ScheduledScript
 */
/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
 **                      
 **@Author      :  Jaydas Sakhare
 **@Dated       :  05 June 2019	
 **@Version     :  2.x
 **@Description :  JDA NS scheduled script to create sales invoice. 
 ***************************************************************************************/
define(['N/record', 'N/search', 'N/runtime', 'N/log', 'N/file', 'N/task'],
    function(record, search, runtime, log, file, task) {

        function execute(scriptContext) {

            var sales_invoice_processed_folder = 25787;//17285;
            try {

                var results = search.load({
                    id: 'customsearch_fc_jda_ns_salesfoldersearch'
                });
				
                //All Files From file cabinet						
                var resultSet = results.run().getRange(0, 60);

                log.debug('Total Files in unpocessed folder==>', resultSet);
				log.debug('Total Files in unpocessed folder==>', resultSet.length);

                //total files loop
                for (var i = 0; i < resultSet.length; i++) {

                    var fileId = resultSet[i].getValue('internalid');
                    log.debug('file id is', fileId);

                    var fileObj = file.load({
                        id: fileId
                    });
                    var fileString = fileObj.getContents();
                    var fileNameOrigin = fileObj.name;
                    var csvTask = task.create({
                        taskType: task.TaskType.CSV_IMPORT
                    });
                    csvTask.mappingId = 'custimport_jda_ns_sales_invoice'; //Saved CSV import Id
                    csvTask.importFile = fileString;
                    var csvTaskId = csvTask.submit();
                    log.audit('csv Task Id', csvTaskId);
					
					var summary = task.checkStatus(csvTaskId);
                        log.audit({
                            title: 'Status******',
                            details: summary
                        });


					if (csvTaskId) {

                        var csvFileObj = file.create({
                            name: fileNameOrigin,
                            fileType: file.Type.CSV,
                            contents: fileString
                        });
                        csvFileObj.folder = sales_invoice_processed_folder; //csv folder id for Item 
                        var csvFileId = csvFileObj.save();
                        log.debug('file move to processed folder -->', csvFileId)

                       
                        file.delete({
                            id: fileId

                        });
                    }

                }

            } catch (e) {


                log.error('Error Message-->', e);
                var errorMessage = e.message;
                // Save the error in error catcher field.
                var erroCatcherObj = record.create({
                    type: 'customrecord_jda_ns_error_catcher'

                });
                erroCatcherObj.setValue({
                    fieldId: 'custrecord_jda_ns_ec_name',
                    value: 'Sales CSV upload error'
                });
                erroCatcherObj.setValue({
                    fieldId: 'custrecord_jda_ns_ec_error',
                    value: errorMessage
                });

                erroCatcherObj.setValue({
                    fieldId: 'custrecord_jda_ns_ec_file_name',
                    value: fileNameOrigin
                });
                var errorid = erroCatcherObj.save();
                log.debug('errorid -->', errorid)
                return e.message;

            }

        }



        return {
            execute: execute
        }

    });