/**
 *@NApiVersion 2.x
 *@NScriptType ScheduledScript
 */
/***************************************************************************************  
 ** Copyright (c) 1998-2018 Softype, Inc.
 ** Ventus Infotech Private Limited, Raheja Plaza One, Suite A201, LBS Marg, Ghatkopar West, Near R City Mall, Mumbai INDIA 400086.
 ** All Rights Reserved.
 ** This software is the confidential and proprietary information of Softype, Inc. ("Confidential Information").
 **You shall not disclose such Confidential Information and shall use it only in accordance with the terms of the license agreement you entered into with Softype.                    
 **                      
 **@Author      :  Jaydas Sakhare
 **@Dated       :  05 June 2019	
 **@Version     :  2.x
 **@Description :  JDA NS scheduled script to create sales invoice from CSV file. 
 **@Updates     :  Completely modified by Nischitha G Y on 17 th May 2022.
 ***************************************************************************************/
define(['N/record', 'N/search', 'N/runtime', 'N/log', 'N/file', 'N/transaction', 'N/format', 'N/task'], function (record, search, runtime, log, file, transaction, format, task) {

    function execute(scriptContext) {

        var scriptObj = runtime.getCurrentScript();
        var lastDocProcess = scriptObj.getParameter({ name: 'custscript_softype_go_mart_jda_ns_credit' });

        var credit_memo_unprocessed_folder = 25795;
        var credit_memo_processed_folder = 25794;
        var invoiceFormId = 172;
        var fileName = '';
        var taxcode = 5;
        var currentDocNo = '';

        if (runtime.envType === 'PRODUCTION') {
            credit_memo_unprocessed_folder = 25795;
            credit_memo_processed_folder = 25794;
            invoiceFormId = 172;
        }

        // Fetch all the files available in sales -> unprocessed folder
        var unprocessedFileSearchObj = search.create({
            type: "file",
            filters: [
                ["folder", "anyof", credit_memo_unprocessed_folder]
            ],
            columns: [
                search.createColumn({
                    name: "internalid",
                    sort: search.Sort.ASC,
                    label: "Internal Id"
                }),
                search.createColumn({ name: "name", label: "Name" }),
                search.createColumn({ name: "filetype", label: "Type" })
            ]
        });

        // Loop through all the files found
        unprocessedFileSearchObj.run().each(function (fileRow) {
            // .run().each has a limit of 4,000 results
            /* Columns Order of CSV file:- 0 Store, 1	Date,	2 Category,3 Category Name,4 Customer Number,5 Customer Name,6 Sales Type,	7 Sales Amount,	8 Document Number,9 Subsidiary */

            var fileId = fileRow.id;
            fileName = fileRow.getValue('name');
            log.debug('fileName', fileName);
            var salesDataFileObj = file.load({
                id: fileId
            });

            // Iterate through lines of CSV file and store data in object of arrays with Document number as its group key.
            var SalesData = {};
            salesDataFileObj.lines.iterator().each(function (line) {
                var data = line.value.split(",");
                if (isNaN(data[0])) {
                    return true;
                }

                if (data[8]) {
                    var tranId = data[8].trim();
                    // log.debug('tranId', tranId);

                    if (SalesData[tranId]) {
                        SalesData[tranId].push(data);
                    } else {
                        SalesData[tranId] = [];
                        SalesData[tranId].push(data);
                    }
                }

                return true;
            });

            // Iterate through sales data and remove the document numbers until the Last Document Process in order to continue from Last Document processed in case of script rescheduled.
            for (var tranId in SalesData) {
                if (lastDocProcess) {
                    if (lastDocProcess == tranId) {
                        break;
                    }
                } else {
                    break;
                }

                delete SalesData[tranId];
            }

            // Iterate through sales data and create invoices with line items
            for (var key in SalesData) {
                if (key == '') {
                    continue;
                }

                var recordData = SalesData[key];

                try {
                    if (recordData.length > 0 && recordData[0]) {

                        var bodyLevelData = recordData[0];
                        if (bodyLevelData[8]) {
                            currentDocNo = bodyLevelData[8].trim();
                        }

                        if (runtime.getCurrentScript().getRemainingUsage() > 100) {
                            // Set Body Level fields in the invoice
                            var creditmemoObj = record.create({
                                type: "creditmemo",
                                isDynamic: true

                            });

                            creditmemoObj.setValue({
                                fieldId: 'customform',
                                value: invoiceFormId
                            });
                            // Document Number
                            if (bodyLevelData[8]) {
                                creditmemoObj.setValue({
                                    fieldId: 'tranid',
                                    value: bodyLevelData[8].trim()
                                });
                            }

                            // log.debug('bodyLevelData[8]', bodyLevelData[8]);
                            // log.debug('bodyLevelData[4]', bodyLevelData[4]);
                            // Customer Number
                            if (bodyLevelData[4]) {
                                creditmemoObj.setText({
                                    fieldId: 'entity',
                                    text: bodyLevelData[4].trim()
                                });
                            }
                            // Date\
                            // log.debug('bodyLevelData[1]', bodyLevelData[1]);

                            if (bodyLevelData[1]) {
                                var trandate = format.parse({
                                    value: bodyLevelData[1].trim(),
                                    type: format.Type.DATE
                                });
                                if (trandate) {
                                    creditmemoObj.setValue({
                                        fieldId: 'trandate',
                                        value: trandate
                                    });
                                }
                            }
                            // log.debug('bodyLevelData[9]', bodyLevelData[9]);

                            // Subsidiary
                            if (bodyLevelData[9]) {
                                creditmemoObj.setValue({
                                    fieldId: 'subsidiary',
                                    value: bodyLevelData[9].trim()
                                });
                            }
                            // log.debug('bodyLevelData[0]', bodyLevelData[0]);

                            // Store
                            if (bodyLevelData[0]) {
                                creditmemoObj.setText({
                                    fieldId: 'location',
                                    text: bodyLevelData[0].trim()
                                });
                            }

                            // log.debug('bodyLevelData[2]', bodyLevelData[2]);

                            // Set Line level fields in the invoice
                            for (var i = 0; i < recordData.length; i++) {
                                var lineLevelData = recordData[i];
                                creditmemoObj.selectLine({
                                    sublistId: 'item',
                                    line: i
                                });
                                if (lineLevelData[2]) {
                                    creditmemoObj.setCurrentSublistText({
                                        sublistId: 'item',
                                        fieldId: 'item',
                                        text: lineLevelData[2].trim(),
                                        ignoreFieldChange: true
                                    });
                                }
                                // log.debug('bodyLevelData[3]', bodyLevelData[3]);

                                if (lineLevelData[3]) {
                                    creditmemoObj.setCurrentSublistValue({
                                        sublistId: 'item',
                                        fieldId: 'description',
                                        value: lineLevelData[3].trim(),
                                        ignoreFieldChange: true
                                    });
                                }
                                // log.debug('bodyLevelData[7]', bodyLevelData[7]);

                                if (lineLevelData[7]) {
                                    creditmemoObj.setCurrentSublistValue({
                                        sublistId: 'item',
                                        fieldId: 'amount',
                                        value: lineLevelData[7].trim(),
                                        ignoreFieldChange: true
                                    });
                                }
                                if (taxcode) {
                                    creditmemoObj.setCurrentSublistValue({
                                        sublistId: 'item',
                                        fieldId: 'taxcode',
                                        value: taxcode,
                                        ignoreFieldChange: true
                                    });
                                }
                                // log.debug('bodyLevelData[6]', bodyLevelData[6]);
                                if (lineLevelData[6]) {
                                    creditmemoObj.setCurrentSublistValue({
                                        sublistId: 'item',
                                        fieldId: 'custcol_jda_sales_type',
                                        value: lineLevelData[6].trim(),
                                        ignoreFieldChange: true
                                    });
                                }
                                creditmemoObj.commitLine({
                                    sublistId: 'item'
                                });
                            }

                            // Save the invoice
                            var creditmemoId = creditmemoObj.save();
                            log.debug('New creditmemoId', creditmemoId);

                        } else {
                            rescheduleCurrentScript(currentDocNo);
                            log.debug('Remaining Usage', runtime.getCurrentScript().getRemainingUsage());
                            return false;
                        }

                    }
                } catch (e) {

                    var errorMessage = e.message;
                    // Create a new error catcher record and save the error details.
                    var erroCatcherObj = record.create({
                        type: 'customrecord_jda_ns_error_catcher'
                    });
                    erroCatcherObj.setValue({
                        fieldId: 'custrecord_jda_ns_ec_name',
                        value: 'Sales CSV upload error'
                    });
                    erroCatcherObj.setValue({
                        fieldId: 'custrecord_jda_ns_ec_error',
                        value: errorMessage
                    });
                    erroCatcherObj.setValue({
                        fieldId: 'custrecord_jda_ns_ec_file_name',
                        value: fileName
                    });
                    erroCatcherObj.setValue({
                        fieldId: 'custrecord_jda_ns_ec_doc_no',
                        value: currentDocNo
                    });
                    var errorid = erroCatcherObj.save();
                    log.debug('errorid -->', errorid);

                }
            }

            if (salesDataFileObj) {
                // Create a copy of the current csv file in processed folder and delete the original file from unprocessed folder.
                var processedSalesDataFileObj = file.create({
                    name: salesDataFileObj.name,
                    fileType: file.Type.CSV,
                    contents: salesDataFileObj.getContents(),
                    folder: credit_memo_processed_folder
                });
                var newFileId = processedSalesDataFileObj.save();
                if (newFileId && fileId) {
                    file.delete({
                        id: fileId
                    });
                }
            }

            return true;
        });

    }

    function rescheduleCurrentScript(lastDocProcess) {
        log.debug('lastDocProcess', lastDocProcess);
        var ssTask = task.create({
            taskType: task.TaskType.SCHEDULED_SCRIPT,
            scriptId: 'customscript_ss_credit_memo',
            deploymentId: 'customdeploy_ss_credit_memo',
            params: {
                custscript_softype_go_mart_jda_ns_credit: lastDocProcess,
            }
        });

        return ssTask.submit();
    }

    return {
        execute: execute
    }

});